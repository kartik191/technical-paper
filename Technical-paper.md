# Domain Driven Design


## Introduction
Domain-Driven Design is a concept introduced by __Eric Evans__ in 2004 in his book _Domain-Driven Design: Tackling Complexity in Heart of Software_. It is an approach for architecting software design by looking at the software in top-down approach. 

According to __Eric Evans__, 
> When we are developing software our focus should not be primarily on technology, rather it should be primarily on business.

So it focuses on three principles:
- The primary focus of the project is the core domain and domain logic.
- Complex designs are based on models of the domain.
- Collaboration between technical and domain experts is crucial to creating an application model that will solve particular domain problems.
## Important terms in Domain-Driven Design
- __Domain__
  - The subject area on which the application that is being developed is called the Domain.
- __Ubiquitous Language__
  - Ubiquitous language is a language shared by the development team and the domain experts. It must be expressed in the Domain Model. It should not use the techincal terms which would not be understood by the domain experts.
- __Bounded context__
  - To deal with a large domain model we can divide the model into different zones which is called Bounded Context.
  For example, an organization can be split into a sales department and a support department, each operating within its context. By this, the work becomes easier and better organized.
- __Entities__
  - Entities are classes where the instances are globally identifiable and keep the same identity for life. There can be change of state in other properties, but the identity never changes.
<!--In short, an entity implements some business logic and could be uniquely identified using an ID.-->
- __Value Objects__
  - These are immutable objects that don’t have any identity. Value objects reduce complexity by performing complex calculations, isolating heavy computational logic from entities.
For example, User is an entity and Address is a value object. Address can change many times but the identity of User never changes. Whenever an Address gets changed, a new Address will be instantiated and assigned to the User.
## Architecture
A typical enterprise application architecture consists of the following four conceptual layers:

![](https://dzone.com/storage/rc-covers/14674-thumb.png)
- **User Interface (Presentation Layer):** This layer is responsible for presenting information to the user and interpreting user commands.
- **Application Layer:** This layer coordinates the application activity. It doesn't contain any business logic. It does not hold the state of business objects, but it can hold the state of an application task's progress.
- **Domain Layer:** This layer contains information about the business domain. The state of business objects is held here. Persistence of the business objects and possibly their state is delegated to the infrastructure layer.
- **Infrastructure Layer:** This layer acts as a supporting library for all the other layers. It provides communication between layers, implements persistence for business objects, contains supporting libraries for the user interface layer, etc.


## Advantages of Domain-Driven Design
1. **Simpler communication:** As the DDD requires Ubiquitous Language, communication between developers and teams becomes much easier and less technical.
2. **Prefers domains over interface:** Since the domain is a primary concept in DDD, developed applications will be accurately suited for the particular domain, as opposed to those applications which emphasize the UI/UX first and foremost. 
3. **More flexibility:** As DDD is object-oriented, everything within the domain model will be based on an object and will be quite modular and encapsulated. Due to this, even the entire system can be altered and improved regularly.

## Disadvantages of Domain-Driven Design
1. **Requires strong domain knowledge:** There has to be at least one domain specialist in the development team who understands the precise characteristics of the subject area. 
2. **Encourages repetitive practices** DDD contains many repetitive practices. It encourages the use of continuous integration to build strong applications that can adapt themselves when necessary. Some organizations may have trouble if their past experience is tied to less-flexible development models.
3. **Not suitable for highly-technical projects:** DDD is perfect for applications that have complex business logic. However, it might not be the best solution for applications with minor domain complexity but high technical complexity, as it can be very challenging for business-oriented domain experts. 

## References
- https://en.wikipedia.org/wiki/Domain-driven_design
- https://www.geeksforgeeks.org/domain-driven-design-ddd/
- https://airbrake.io/blog/software-design/domain-driven-design
- https://medium.com/microtica/the-concept-of-domain-driven-design-explained-3184c0fd7c3f
- https://www.youtube.com/watch?v=NNFJREcalc0
- https://www.youtube.com/watch?v=pMuiVlnGqjk
- https://towardsdatascience.com/what-is-domain-driven-design-5ea1e98285e4
- https://dzone.com/refcardz/getting-started-domain-driven
